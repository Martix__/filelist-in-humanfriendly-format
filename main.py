import os


path = input("Geef directory van listing op: ")

with os.scandir(path) as lijstvanFiles:
    for bestand in lijstvanFiles:                           #Voor elk bestand dat zich in deze directory bevindt
        if bestand.is_file():                               #Als dat bestand een file is
            print(bestand.name)                             #Wordt deze hieronder geprint
            if os.access(bestand, os.R_OK) == True:                  #Als deze file read permissions toestaat
                print("Deze file biedt lees toestemming")           #Print dat dan hieronder

            if os.access(bestand, os.W_OK) == True:                      #Als deze file write permissions toestaat
                print("Deze file biedt schrijf toestemming")        #Print dat dan hieronder

            if os.access(bestand, os.X_OK) == True:                      #Als deze file execute permissions toestaat
                        print("Deze file biedt execute toestemming")  #Print dat dan hieronder

            else:                               #Else (Wanneer er dus helemaal niks van hierboven van toepassing is)
                print("Deze file biedt geen schrijf, lees of execute toestemming") #Print dat dan ook